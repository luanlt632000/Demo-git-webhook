const express = require("express");
const app = express();
const cors = require("cors");
app.use(cors());

app.get("/api/products", (req, res) => {
  const data = [
    
    {
      PID: "N9K-C9372PX",
      VID: "V03",
      SN: "SAL1948TVZW",
      line: [100, 200, 300],
      fileName: "20230929-TEST01-Session.Port6-8-172.16.20.7.log",
      warehouse: "AU",
    },
    {
      PID: "N9K-PAC-650W",
      VID: "V01",
      SN: "DCB2010X2EA",
      line: [125, 225, 325],
      fileName: "20230929-TEST01-Session.Port6-8-172.16.20.7.log",
      warehouse: "AU",
    },
    {
      PID: "N9K-PAC-650W",
      VID: "V01",
      SN: "DCB2010X2GW",
      line: [150, 250, 350],
      fileName: "20230929-TEST01-Session.Port6-8-172.16.20.7.log",
      warehouse: "AU",
    },
  ];
  res.status(200).send(data);
});

const PORT = 8000;

app.listen(PORT, () => {
  console.log("Server is running on port ", PORT);
});

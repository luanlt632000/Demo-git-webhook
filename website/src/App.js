import React, { useEffect, useState } from "react";
import axios from "axios";
import "./App.css";
const App = () => {
  const [allProducts, setAllProducts] = useState([]);

  const getProducts = async () => {
    try {
      const res = await axios.get("http://172.16.2.99/api/products");

      setAllProducts(res.data);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getProducts();
  }, []);
  return (
    <div className="App">
      <h1>Demo Git Webhook</h1>
      {allProducts.map((pro) => (
        <div>
          <h3 style={{ backgroundColor: "yellow" }}>
            SN: {pro.SN}
          </h3>
          <p>PID: {pro.PID}</p>
          <p>VID: {pro.VID}</p>
          <p>File: {pro.fileName}</p>
          <p>Warehouse: {pro.warehouse}</p>
          <br></br>
        </div>
      ))}
    </div>
  );
};

export default App;
